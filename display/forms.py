from django import forms
from bo.models import Suivis

class SuivisForm(forms.ModelForm):
    class Meta:
        model = Suivis
        fields = ['estim_tech', 'estim_social', 'desc',]
        widgets = {
            'desc': forms.Textarea(attrs={'rows': 6}),
        }
