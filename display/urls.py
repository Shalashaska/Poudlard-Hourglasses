from django.conf.urls import url, include
import django.contrib.auth.views as auth_views
from . import views

urlpatterns = [
    url(r'^login/$', views.login, name='login'),
    url(r'^logout/$', views.logout, name='logout'),
    url(r'^display/$', views.index, name='index'),
]
