from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.template import loader
from bo import models
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout

from datetime import datetime, timedelta
from bo.models import ModuleRegister, IndividualProfile, Promotion, Activity, GroupMember, NsLog, Module, Event
from django.utils import timezone
from django.contrib.auth.forms import AuthenticationForm
from display.forms import SuivisForm


def index(request):
     return render(request, 'display/index.html')

def login(request):
     if request.user.is_authenticated():
          return HttpResponseRedirect(reverse(suivis))
     else:
          form = AuthenticationForm(None, request.POST or None)
          if form.is_valid():
               auth_login(request, form.get_user())
               return HttpResponseRedirect(reverse(suivis))
          return render(request, 'registration/login.html', {'form' : form})

def logout(request):
     if request.user.is_authenticated():
          auth_logout(request)
          return render(request, 'registration/logout.html')
     else:
          return render(request, 'registration/logout.html')
