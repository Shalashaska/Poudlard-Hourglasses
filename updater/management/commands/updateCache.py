from django.core.management.base import BaseCommand, CommandError
from updater.update import update

class Command(BaseCommand):
    help = 'update WardenWS Cache'

    def add_arguments(self, parser):
        pass
#        parser.add_argument('poll_id', nargs='+', type=int)

    def handle(self, *args, **options):
        update()
        

