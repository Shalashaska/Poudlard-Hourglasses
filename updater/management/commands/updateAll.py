from django.core.management.base import BaseCommand, CommandError
from updater.update import updateAll

class Command(BaseCommand):
    help = 'update All WardenWS Cache'

    def add_arguments(self, parser):
        pass
#        parser.add_argument('poll_id', nargs='+', type=int)

    def handle(self, *args, **options):
        updateAll()
        

