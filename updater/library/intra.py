import http.cookiejar
import urllib.parse
import urllib.request
import json
import time

INTRA_URL = "https://intra.epitech.eu/"
JSON_SUFFIX = "?format=json"
LOCATION = "FR/PAR"

def sendRequest(target, count = 0):
    print(target)
    try:
        request = urllib.request.Request(target)
        with urllib.request.urlopen(request) as response:
            data = response.read()
    except:
        if (count < 10):
            print("couldn't connect, retrying in 10 sec ...(try {0}/10)".format(str(count)))
            count += 1
            time.sleep(10)
            return sendRequest(target, count)
        else:
            print("maximum request reached")
    else:
        return data

def getDataFromUrl(url, **kwargs):
    suffix = ""
    first = True
    for key, val in kwargs.items():
        if first:
            suffix += "?"
            first = False
        else:
            suffix += "&"
        if type(val) is list:
            firstlist = True
            for item in val:
                if firstlist == True:
                    firstlist = False
                else:
                    suffix += "&"
                suffix += "%s[]=%s" % (key, item)
        else:
            suffix += "%s=%s" % (key, val)
    data = sendRequest(url + suffix)
    return data

def toJson(data):
    return json.loads(data.decode('utf-8'))    

class DataExtractor:
    def __init__(self):
        self.cj = http.cookiejar.CookieJar()
        self.opener =  urllib.request.build_opener(urllib.request.HTTPCookieProcessor(self.cj))
        self.opener.addheaders = [('Content-Type', 'application/json'), ('Accept','text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8')]
        urllib.request.install_opener(self.opener)

    def logIn(self, login, password):
        print("Authenticating to " + INTRA_URL + " ...")
        payload = {
            'login': login,
            'password': password
        }
        data = urllib.parse.urlencode(payload).encode("utf-8")
        req = urllib.request.Request(INTRA_URL + JSON_SUFFIX, data)
        try:
            resp = urllib.request.urlopen(req)
        except(urllib.request.HTTPError) as e:
            print("Can't connect to the intranet : " + str(e.code))
        else:
            print("Successfully logged in !")

    def getAllGroupFromActivity(self, activity):
        #https://intra.epitech.eu/module/2015/B-CPE-155/PAR-2-1/acti-208403/registered?format=json
        url = "{0}module/{1}/{2}/{3}/{4}/registered/".format(
            str(INTRA_URL),
            str(activity.module.scolaryear),
            str(activity.module.code),
            str(activity.module.codeinstance),
            str(activity.codeacti)
        )
        data = getDataFromUrl(url, format="json")
        return toJson(data)

    def getAllNoteFromActivity(self, activity):
        #https://intra.epitech.eu/module/2015/B-CPE-155/PAR-2-1/acti-208403/note?format=json
        url = "{0}module/{1}/{2}/{3}/{4}/note/".format(
            str(INTRA_URL),
            str(activity.module.scolaryear),
            str(activity.module.code),
            str(activity.module.codeinstance),
            str(activity.codeacti)
        )
        data = getDataFromUrl(url, format="json")
        return toJson(data)

    def getAllLocations(self):
        url = "{0}user/filter/location/".format(
            str(INTRA_URL)
            )
        data = getDataFromUrl(url, format="json", active="true")
        return toJson(data)

    def getAllRegisteredFromModule(self, module):
        url = "{0}module/{1}/{2}/{3}/registered/".format(
            str(INTRA_URL),
            str(module.scolaryear),
            str(module.code),
            str(module.codeinstance)
            )
        data = getDataFromUrl(url, format="json")
        return toJson(data)

    def getAllActivityFromModule(self, module):
        url = "{0}module/{1}/{2}/{3}/".format(
            str(INTRA_URL),
            str(module.scolaryear),
            str(module.code),
            str(module.codeinstance)
        )
        data = getDataFromUrl(url, format="json")
        return toJson(data)['activites']

    def getAllModuleFromLocation(self, location, scolaryear):
        url = "{0}course/filter/".format(
            str(INTRA_URL)
            )
        data = getDataFromUrl(url, format="json", location=[location.code,], scolaryear=[scolaryear,])
        return toJson(data)

    def getAllPromotionsFromCourse(self, course):
        url = "{0}user/filter/promo/".format(
            str(INTRA_URL)
        )
        data = getDataFromUrl(url, format="json", course=course.code, location=course.location.code, year=course.scolaryear, active="true")
        return toJson(data)

    def getAllStudentsFromPromotion(self, promotion):
        def getStudents(promotion, offset=0):
            course = promotion.course
            return getDataFromUrl(url, format="json", location=course.location.code, course=course.code, year=course.scolaryear, active="true", promo=promotion.promo, offset=offset)
        url = "{0}user/filter/user/".format(
            str(INTRA_URL)
        )
        students = []
        total = int()
        data = getStudents(promotion)
        jsonData = toJson(data)
        students += jsonData['items']
        total = jsonData['total']
        while (len(students) != total):
            data = getStudents(promotion, len(students))
            jsonData = toJson(data)
            students += jsonData['items']
        return students

    def getAllLogFromIndividual(self, individual):
        url = "{0}user/{1}/netsoul/".format(
            str(INTRA_URL),
            str(individual.login)
        )
        data = getDataFromUrl(url, format="json")
        return toJson(data)

    def getAllSuivisFromIndividual(self, individual):
        url = "{0}user/{1}/comments/list".format(
            str(INTRA_URL),
            str(individual.login)
        )
        data = getDataFromUrl(url, format="json", offset=0, types=['suivi_adm', 'suivi_prof', 'suivi_mail',])
        return toJson(data)

    def getAllConventions(self):
        url = "{0}stage/load".format(
            str(INTRA_URL),
        )
        data = getDataFromUrl(url, format="json")
        return toJson(data)['items']

    def getAllCourseFromLocation(self, location, scolaryear):
        # https://intra.epitech.eu/user/filter/course?format=json&location=FR/PAR&year=2015&active=true
        url = "{0}user/filter/course/".format(
            str(INTRA_URL)
        )
        data = getDataFromUrl(url, format="json", location=location.code, year=scolaryear, active="true")
        return toJson(data)
