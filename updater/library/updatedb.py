from django.utils import timezone
from bo.serializers import LocationSerializer, PromotionSerializer, IndividualProfileSerializer, ModuleSerializer, ModuleRegisterSerializer, ActivitySerializer, GroupSerializer, GroupMemberSerializer, GroupSerializer, SuivisSerializer, ConventionSerializer, EventSerializer, NoteSerializer, CourseSerializer
from bo.models import Promotion, Location, IndividualProfile, Module, ModuleRegister, Activity, Group, GroupMember, NsLog, Suivis, Convention, Event, Note, Course
from django.db.models import Q
from datetime import datetime, time, timedelta
import pytz
from django.db import transaction
from warden.settings import SCOLARYEAR

def updateAllCourse(extractor):
     print("Updating promotions")
     location = Location.objects.get(title = "Paris")
     updateCourse(location, SCOLARYEAR, extractor)

def updateAllSuivis(extractor):
    course = Course.objects.get(code = "bachelor/classic")
    promotion = Promotion.objects.get(course = course, promo = 'tek1')
    students = IndividualProfile.objects.filter(promotion = promotion)
    for student in students:
         updateStudentSuivis(student, extractor)

def updateAllLog(extractor):
    course = Course.objects.get(code = "bachelor/classic")
    promotion = Promotion.objects.get(course = course, promo = 'tek1')
    students = IndividualProfile.objects.filter(promotion = promotion)
    for student in students:
         updateStudentLog(student, extractor)

def updateAllPromotion(extractor):
     print("Updating promotions")
     location = Location.objects.get(title = "Paris")
     courses = Course.objects.filter(location = location)
     for course in courses:
          updatePromotion(course, extractor)

def updateAllStudent(extractor):
     print("Updating student profiles")
     course = Course.objects.get(code = "bachelor/classic")
     promotion = Promotion.objects.get(course = course, promo = 'tek1')
     updateStudent(promotion, extractor)

def updateAllModule(extractor):
     print("Updating module list")
     location = Location.objects.get(title = "Paris")
     updateModule(location, extractor)

def updateAllModuleRegister(extractor):
     print("Updating module suscribed students")
     modules = Module.objects.all()
     for module in modules:
          updateModuleRegister(module, extractor)

def updateAllActivity(extractor):
     print("Updating activities")
     modules = Module.objects.all()
     for module in modules:
          updateActivity(module, extractor)

def updateAllActivityGroup(extractor):
     print("Updating activity group")
     modules = Module.objects.all()
     for module in modules:
          activities = Activity.objects.filter(module = module)
          for activity in activities:
               updateActivityGroup(activity, extractor)     

def updateAllActivityNote(extractor):
     print("Updating activity note")
     modules = Module.objects.all()
     for module in modules:
          activities = Activity.objects.filter(module = module)
          for activity in activities:
               updateActivityNote(activity, extractor)

@transaction.atomic
def updateAllConventions(extractor):
     jsonData = extractor.getAllConventions()
     for jsonConvention in jsonData:
          try:
               student = IndividualProfile.objects.get(login = jsonConvention['login'])
          except(IndividualProfile.DoesNotExist):
               pass
          else:
               convention, created = Convention.objects.get_or_create(hash = jsonConvention['hash'])
               if (created == True):
                    convention.individual = student
               serializer = ConventionSerializer(convention, data=jsonConvention)
               if (serializer.is_valid()):
                    serializer.save()
               else:
                    print(serializer.errors)
               

@transaction.atomic
def updateGroupMember(individual, group, groupMemberJson):
     groupMember, _ = GroupMember.objects.get_or_create(individual=individual, group=group)
     groupMemberSerializer = GroupMemberSerializer(groupMember, data=groupMemberJson)
     if (groupMemberSerializer.is_valid()):
          groupMemberSerializer.save()
          print(str(groupMember))
     else:
          print(groupMemberSerializer.errors)

def getMember(member):
     try:
          individual = IndividualProfile.objects.get(login=member['login'])
     except (IndividualProfile.DoesNotExist):
          print("%s's login is not in the database" % member['login'])
     else:
          return individual
     return None

@transaction.atomic
def updateActivityGroup(activity, extractor):
     jsonData = extractor.getAllGroupFromActivity(activity)
     for jsonGroup in jsonData:
          members = []
          member = getMember(jsonGroup['master'])
          if (member != None):
               members.append((jsonGroup['master'], member))
          for memberGroup in jsonGroup['members']:
               member = getMember(memberGroup)
               if (member != None):
                    members.append((memberGroup, member))

          if (len(members) > 0):
               group, _ = Group.objects.get_or_create(id = jsonGroup['id'])
               print("Creating group of id %s for activity %s." % (str(jsonGroup['id']), str(activity)))
               group.activity = activity
               serializer = GroupSerializer(group, data=jsonGroup)
               for member in members:
                    updateGroupMember(member[1], group, member[0])
               if (serializer.is_valid()):
                    serializer.save()
               else:
                    print(serializer.errors)         
               

@transaction.atomic
def updateModuleRegister(module, extractor):
     jsonData = extractor.getAllRegisteredFromModule(module)
     for jsonModuleRegister in jsonData:
          try:
               individual = IndividualProfile.objects.get(login = jsonModuleRegister['login'])
          except (IndividualProfile.DoesNotExist):
               print(jsonModuleRegister['login'] + " is not a in the db")
          else:
               register, _ = ModuleRegister.objects.get_or_create(individual = individual, module = module)
               register.module = module
               serializer = ModuleRegisterSerializer(register, data=jsonModuleRegister)
               if (serializer.is_valid()):
                    serializer.save()
               else:
                    print(serializer.errors)

@transaction.atomic
def updateActivity(module, extractor):
     jsonData = extractor.getAllActivityFromModule(module)
     for jsonActivity in jsonData:
          activity, _ = Activity.objects.get_or_create(codeacti = jsonActivity['codeacti'], module = module)
          updateActivityEvents(activity, jsonActivity['events'])
          activity.module = module
          serializer = ActivitySerializer(activity, data=jsonActivity)
          if (serializer.is_valid()):
               serializer.save()
          else:
               print(serializer.errors)
     

@transaction.atomic
def updateActivityNote(activity, extractor):
     jsonData = extractor.getAllNoteFromActivity(activity)
     for jsonNote in jsonData:
          try:
               individual = IndividualProfile.objects.get(login = jsonNote['login'])
          except (IndividualProfile.DoesNotExist):
               print(jsonNote['login'] + " is not a in the db")
          else:
               note, _ = Note.objects.get_or_create(individual = individual, activity = activity)
               serializer = NoteSerializer(note, data=jsonNote)
               if (serializer.is_valid()):
                    serializer.save()
               else:
                    print(serializer.errors)

@transaction.atomic
def updateModule(location, extractor):
     jsonData = extractor.getAllModuleFromLocation(location, SCOLARYEAR)
     for jsonModule in jsonData:
          module, _ = Module.objects.get_or_create(id = jsonModule['id'])
          module.location = location
          serializer = ModuleSerializer(module, data=jsonModule)
          if (serializer.is_valid()):
               serializer.save()
          else:
               print(serializer.errors)    
     
    
@transaction.atomic
def updateLocation(extractor):
     jsonData = extractor.getAllLocations()
     for jsonLocation in jsonData:
          location, _ = Location.objects.get_or_create(title = jsonLocation['title'])
          serializer = LocationSerializer(location, data=jsonLocation)
          if (serializer.is_valid()):
               serializer.save()
          else:
               print(serializer.errors)
     

@transaction.atomic
def updatePromotion(course, extractor):
     jsonData = extractor.getAllPromotionsFromCourse(course)
     for jsonPromotion in jsonData:
          promotion, _ = Promotion.objects.get_or_create(promo = jsonPromotion['promo'], course = course)
          serializer = PromotionSerializer(promotion, data=jsonPromotion)
          if (serializer.is_valid()):
               serializer.save()
          else:
               print(serializer.errors)
     

@transaction.atomic
def updateStudent(promotion, extractor):
     jsonData = extractor.getAllStudentsFromPromotion(promotion)
     for jsonStudent in jsonData:
          student, _ = IndividualProfile.objects.get_or_create(login = jsonStudent['login'])
          student.promotion = promotion
          serializer = IndividualProfileSerializer(student, data=jsonStudent)
          if (serializer.is_valid()):
               serializer.save()
          else:
               print(serializer.errors)
     

@transaction.atomic
def updateStudentLog(student, extractor):
     jsonData = extractor.getAllLogFromIndividual(student)
     for jsonLog in jsonData:
          date = datetime.utcfromtimestamp(jsonLog[0]).replace(tzinfo=pytz.utc)
          nslog, _ = NsLog.objects.get_or_create(individual = student, date = date)
          nslog.time = timedelta(seconds = jsonLog[1])
          nslog.save()

@transaction.atomic
def updateLastStudentLog(student, extractor):
     jsonData = extractor.getAllLogFromIndividual(student)
     try:
          log = NsLog.objects.filter(individual = student).latest('date')
     except (NsLog.DoesNotExist):
          for jsonLog in jsonData:
               date = datetime.utcfromtimestamp(jsonLog[0]).replace(tzinfo=pytz.utc)
               nslog, _ = NsLog.objects.get_or_create(individual = student, date = date)
               nslog.time = timedelta(seconds = jsonLog[1])
               nslog.save()
     else:
          date = timezone.now()
          diff = date - log.date
          for jsonLog in jsonData[:diff.days]:
               date = datetime.utcfromtimestamp(jsonLog[0]).replace(tzinfo=pytz.utc)
               nslog, _ = NsLog.objects.get_or_create(individual = student, date = date)
               nslog.time = timedelta(seconds = jsonLog[1])
               nslog.save()

@transaction.atomic
def updateStudentSuivis(student, extractor):
     jsonData = extractor.getAllSuivisFromIndividual(student)
     for jsonSuivis in jsonData:
          suivis, created = Suivis.objects.get_or_create(id_intra = jsonSuivis['id'])
          owner, _ = IndividualProfile.objects.get_or_create(login = jsonSuivis['login'])
          if created == True:
               suivis.individual = student
               suivis.owner = owner
          serializer = SuivisSerializer(suivis, data=jsonSuivis)
          if (serializer.is_valid()):
               serializer.save()
          else:
               print(serializer.errors)

@transaction.atomic
def updateActivityEvents(activity, jsonData):
     for jsonEvent in jsonData:
          event, created = Event.objects.get_or_create(code = jsonEvent['code'], activity = activity)
          serializer = EventSerializer(event, data=jsonEvent)
          if (serializer.is_valid()):
               serializer.save()
          else:
               print(serializer.errors)

@transaction.atomic
def updateCourse(location, scolaryear, extractor):
     jsonData = extractor.getAllCourseFromLocation(location, scolaryear)
     for jsonCourse in jsonData:
          course, _ = Course.objects.get_or_create(code = jsonCourse['code'], location = location, scolaryear = scolaryear)
          serializer = CourseSerializer(course, data=jsonCourse)
          if (serializer.is_valid()):
               serializer.save()
          else:
               print(serializer.errors)     

