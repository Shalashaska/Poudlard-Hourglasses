from django.contrib import admin
from bo.models import Location, IndividualProfile, Promotion, Module, ModuleRegister, Activity, Group, NsLog, Suivis, Convention, Event

# Register your models here.

class ModuleAdmin(admin.ModelAdmin):
    list_display = ('id', 'code', 'codeinstance', 'title', 'begin', 'end', 'end_register',)
admin.site.register(Module, ModuleAdmin)

class NsLogAdmin(admin.ModelAdmin):
    list_display = ('individual', 'date', 'time')
admin.site.register(NsLog, NsLogAdmin)

class GroupAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'closed', 'activity', 'final_note',)
    filter_horizontal = ('members',)
admin.site.register(Group, GroupAdmin)

class ActivityAdmin(admin.ModelAdmin):
    list_display = ('codeacti', 'title', 'module', 'start', 'end',)
admin.site.register(Activity, ActivityAdmin)

class ModuleRegisterAdmin(admin.ModelAdmin):
    list_display = ('individual', 'module', 'credits', 'grade', 'date_ins',)
admin.site.register(ModuleRegister, ModuleRegisterAdmin)

class LocationAdmin(admin.ModelAdmin):
    list_display = ('id', 'code', 'title',)
admin.site.register(Location, LocationAdmin)

class PromotionAdmin(admin.ModelAdmin):
    list_display = ('id', 'promo', 'students','course',)
admin.site.register(Promotion, PromotionAdmin)

class IndividualProfileAdmin(admin.ModelAdmin):
    list_display = ('id', 'login', 'prenom', 'nom', 'promotion', 'date_update', 'date_creation',)
admin.site.register(IndividualProfile, IndividualProfileAdmin)

class SuivisAdmin(admin.ModelAdmin):
    list_display = ('owner', 'individual', 'date', 'desc',)
admin.site.register(Suivis, SuivisAdmin)

class ConventionAdmin(admin.ModelAdmin):
    list_display = ('individual', 'date_creation', 'status',)
admin.site.register(Convention, ConventionAdmin)

class EventAdmin(admin.ModelAdmin):
    list_display = ('code', 'activity', 'begin', 'end', 'seats',)
admin.site.register(Event, EventAdmin)
