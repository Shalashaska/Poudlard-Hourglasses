from django.utils import timezone
from updater.library.intra import DataExtractor
from updater.library import updatedb
from apscheduler.schedulers.blocking import BlockingScheduler
from datetime import datetime, timedelta
from bo.models import ModuleRegister, IndividualProfile, Promotion, Activity, NsLog, GroupMember, Group, Convention

cycle = []
current = 0
update_delay = 4

def init_table():
#     cycle.append((updatedb.updateLocation, "locations"))
 #    cycle.append((updatedb.updateAllCourse, "course"))     
  #   cycle.append((updatedb.updateAllPromotion, "promotions"))
   #  cycle.append((updatedb.updateAllStudent, "student profiles"))
#     cycle.append((updatedb.updateAllLog, "student nslogs"))
#     cycle.append((updatedb.updateAllModule, "modules"))
#     cycle.append((updatedb.updateAllModuleRegister, "module registrations"))
#     cycle.append((updatedb.updateAllActivity, "activity"))
#     cycle.append((updatedb.updateAllActivityGroup, "activity registrations"))
#     cycle.append((updatedb.updateAllActivityNote, "activity note"))
#     cycle.append((updatedb.updateAllSuivis, "student follow up"))
     cycle.append((updatedb.updateAllConventions, "conventions"))

def update_cycle():
     global current
     global cycle
     global update_delay
     print("Update {0}/{1}\nUpdating {2} ...".format(str(current + 1), str(len(cycle)), str(cycle[current][1])))
     extractor = DataExtractor()
     extractor.logIn("morosi_a", "PXGT6vym")
     try:
          cycle[current][0](extractor)
     except:
          print("Error with previous action.")
     else:
          current = (current + 1) % len(cycle)
          print("Update complete, updating {0} in {1} hours ...".format(str(cycle[current][1]),str(update_delay)))
          
def updateAll():
     init_table()
     extractor = DataExtractor()
     extractor.logIn("morosi_a", "PXGT6vym")
     for item in cycle:
          item[0](extractor)
     exit()

def test():
     extractor = DataExtractor()
     extractor.logIn("morosi_a", "PXGT6vym")
     updatedb.updateAllActivityNote(extractor)
     exit()

     
def update():
     init_table()
#     updateAll()
#     test()
     scheduler = BlockingScheduler()
     scheduler.add_job(update_cycle, 'interval', hours=update_delay, id='update_cycle', next_run_time=datetime.now())
     scheduler.start()
