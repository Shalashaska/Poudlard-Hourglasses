# -*- coding: utf-8 -*-

from django.db import models
# Create your models here.

class Location(models.Model):
    code = models.CharField(max_length=10,null=False,blank=False, unique=True)
    title = models.CharField(max_length=20,null=False,blank=False)
    date_creation = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.title)

    def getPromotions(self):
        promotion = Promotion.objects.filter(location = self)
        return students
        
class Course(models.Model):
    code = models.CharField(max_length=100,null=False,blank=False, unique=True)
    title = models.CharField(max_length=100,null=False,blank=False, unique=True)
    location = models.ForeignKey(Location, null=True)
    scolaryear = models.DecimalField(max_digits=4, decimal_places=0, null=True)
    date_creation = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.code) + " " + str(self.title)

class Promotion(models.Model):
    students = models.DecimalField(max_digits=4, decimal_places=0, null=True)
    promo = models.CharField(max_length=20,null=False,blank=False)
    course = models.ForeignKey(Course, null=True)
    date_creation = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.promo)

    def getStudents(self):
        students = IndividualProfile.objects.filter(promotion = self)
        return students
 
class IndividualProfile(models.Model):
    login = models.CharField(max_length=8,null=False,blank=False, unique=True)
    nom = models.CharField(max_length=100,null=True,blank=False)
    prenom = models.CharField(max_length=100,null=True,blank=False)
    promotion = models.ForeignKey(Promotion, null=True)

    date_creation = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    def getModules(self):
        modules = ModuleRegister.objects.filter(individual=self)
        return modules

    def getSuivis(self):
        suivis = Suivis.objects.filter(individual=self).order_by('-date')
        return suivis

    def getNsLogs(self, nbr):
        nslogs = NsLog.objects.filter(individual=self).order_by('-date')[:nbr]
        return nslogs

    def getEmail(self):
        return ("{0}.{1}@epitech.eu".format(str(self.prenom), str(self.nom)))

    def getCredits(self):
        modules = self.getModules()
        credits = 0
        for module in modules:
            credits += module.credits
        return credits

    def __str__(self):
        return "[" +str(self.login)+ "] " + str(self.prenom) + " " + str(self.nom)

class IndividualData(models.Model):
    location = models.CharField(max_length=10,null=True,blank=False)
    close = models.BooleanField()
    studentyear = models.DecimalField(max_digits=1, decimal_places=0)
    credits = models.DecimalField(max_digits=7, decimal_places=0)
    profile = models.ForeignKey(IndividualProfile, null=True)
    date_creation = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    
class ModuleRegister(models.Model):
    grade = models.CharField(max_length=15,null=True,blank=False)
    date_ins = models.DateTimeField(null=True)
    credits = models.DecimalField(max_digits=2, decimal_places=0, null=True)
    individual = models.ForeignKey(IndividualProfile, null=True)
    module = models.ForeignKey('Module', null=True)
    date_creation = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.module) + " " + str(self.individual)

class Note(models.Model):
    comment = models.CharField(max_length=1500,null=True,blank=False)
    date = models.DateTimeField(null=True)
    note = models.DecimalField(max_digits=10, decimal_places=3, null=True)
    individual = models.ForeignKey(IndividualProfile, null=True)
    activity = models.ForeignKey('Activity', null=True)
    date_creation = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.activity) + " " + str(self.individual) + " " + str(self.note)
    
class Module(models.Model):
    id = models.DecimalField(max_digits=8, decimal_places=0, unique=True, primary_key=True)
    semester = models.DecimalField(max_digits=2, decimal_places=0, null=True)
    scolaryear = models.DecimalField(max_digits=4, decimal_places=0, null=True)
    credits = models.DecimalField(max_digits=2, decimal_places=0, null=True)
    title = models.CharField(max_length=100,null=True,blank=False)
    codeinstance = models.CharField(max_length=20,null=True,blank=False)
    code = models.CharField(max_length=20,null=True,blank=False)
    location = models.ForeignKey(Location, null=True)
    begin = models.DateField(null=True)
    end = models.DateField(null=True)
    end_register = models.DateField(null=True)
    open = models.NullBooleanField(null=True)
    suscribed = models.ManyToManyField(IndividualProfile, through=ModuleRegister)
    date_creation = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.code)

    def getActivities(self):
        activities = Activity.objects.filter(module=self)
        return activities

class Activity(models.Model):
    codeacti = models.CharField(max_length=100,null=True,blank=False)
    title = models.CharField(max_length=100,null=True,blank=False)
    type_title = models.CharField(max_length=20,null=True,blank=False)
    type_code = models.CharField(max_length=20,null=True,blank=False)
    begin = models.DateTimeField(null=True)
    start = models.DateTimeField(null=True)
    end_register = models.DateTimeField(null=True)
    deadline = models.DateTimeField(null=True)
    end = models.DateTimeField(null=True)
    nb_hours = models.TimeField(null=True)
    is_projet = models.NullBooleanField(null=True)
    is_note = models.NullBooleanField(null=True)
    module = models.ForeignKey(Module, null=True)
    date_creation = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.codeacti)

class Event(models.Model):
    code = models.CharField(max_length=20,null=True,blank=False)
    seats = models.DecimalField(max_digits=6, decimal_places=0, null=True)
    begin = models.DateTimeField(null=True)
    end = models.DateTimeField(null=True)
    location = models.CharField(max_length=200,null=True,blank=False)
    activity = models.ForeignKey(Activity, null=True, related_name="events")

class GroupMember(models.Model):
    date_ins = models.DateTimeField(null=True)
    status = models.CharField(max_length=20,null=True,blank=False)
    individual = models.ForeignKey(IndividualProfile, null=True)
    group = models.ForeignKey("Group", null=True)

    def __str__(self):
        return str(self.individual) + " " +  str(self.status)
    
class Group(models.Model):
    id = models.DecimalField(max_digits=8, decimal_places=0, unique=True, primary_key=True)
    title = models.CharField(max_length=100,null=True,blank=True)
    final_note = models.DecimalField(max_digits=5, decimal_places=1, null=True)
    closed = models.NullBooleanField(null=True)
    members = models.ManyToManyField(IndividualProfile, related_name="group", through=GroupMember)
    activity = models.ForeignKey(Activity, null=True, related_name="groups")
    date_creation = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.title) + " " +  str(self.activity)

class NsLog(models.Model):
    date = models.DateTimeField(null=True)
    time = models.DurationField(null=True)
    individual = models.ForeignKey(IndividualProfile, null=True)
    
    date_creation = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.date) + " " +  str(self.time) + " " + str(self.individual)

class Suivis(models.Model):
    id_intra = models.DecimalField(max_digits=14, decimal_places=0, unique=True, null=True, blank=True)
    TECHNICAL_LEVEL = (
        (0, 'Inconnu'),
        (1, 'Très bon'),
        (2, 'Bon'),
        (3, 'Moyen'),
        (4, 'Faible'),
        (5, 'Très faible'),
    )
    SOCIAL_LEVEL = (
        (0, 'Inconnue'),
        (1, 'Bonne'),
        (2, 'Correcte'),
        (3, 'Problématique'),
    )

    estim_tech = models.DecimalField(max_digits=1, decimal_places=0, choices=TECHNICAL_LEVEL, default=0)
    estim_social = models.DecimalField(max_digits=1, decimal_places=0, choices=SOCIAL_LEVEL, default=0)
    desc = models.CharField(max_length=10000,null=True,blank=True)
    date = models.DateTimeField(null=True)
    type = models.CharField(max_length=30,null=True,blank=True)
    owner = models.ForeignKey(IndividualProfile, null=True, related_name="owned_suivis")
    individual = models.ForeignKey(IndividualProfile, null=True, related_name="suivis")

    date_creation = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.date) + " " +  str(self.owner) + " " + str(self.individual)

class Convention(models.Model):
    individual = models.ForeignKey(IndividualProfile, null=True, related_name="convention")
    status = models.CharField(max_length=30,null=True,blank=True)
    hash = models.CharField(max_length=10,null=True,blank=True)
    school_year = models.CharField(max_length=10,null=True,blank=True)
    date_creation = models.DateTimeField(null = True)
    
    def __str__(self):
        return str(self.date_creation) + " " +  str(self.individual) + " " + str(self.school_year) + " " + str(self.status)
