from rest_framework import serializers
from bo.models import IndividualProfile, IndividualData, Location, Promotion, Module, ModuleRegister, Activity, Group, GroupMember, Suivis, Convention, Event, Note, Course

#################################### Individual ####################################


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ('id', 'title', 'final_note', 'closed',)

class GroupMemberSerializer(serializers.ModelSerializer):
    class Meta:
        model = GroupMember
        fields = ('date_ins', 'status', 'individual',)

class PromotionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Promotion
        fields = ('students', 'promo',)

class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = ('code', 'title',)

class ActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Activity
        fields = ('codeacti', 'title', 'type_title', 'type_code', 'begin', 'start', 'end', 'end_register', 'deadline', 'nb_hours', 'is_projet', 'is_note', 'module',)
    
class ModuleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Module
        fields = ('id', 'title', 'code', 'codeinstance', 'credits', 'begin', 'end', 'end_register', 'semester', 'open', 'scolaryear',)

class ModuleRegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = ModuleRegister
        fields = ('grade', 'date_ins', 'credits')

class NoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Note
        fields = ('note', 'date', 'comment',)

class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = ('code', 'title',)

class IndividualDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = IndividualData
        fields = ('promo', 'location', 'close', 'studentyear', 'credits')

class IndividualProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = IndividualProfile
        fields = ('login', 'prenom', 'nom',)

class SuivisSerializer(serializers.ModelSerializer):
    class Meta:
        model = Suivis
        fields = ('desc', 'date', 'type',)

class ConventionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Convention
        fields = ('status', 'school_year', 'date_creation',)

class EventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = ('code', 'seats', 'begin', 'end', 'location',)
