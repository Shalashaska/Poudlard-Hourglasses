# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-04-15 09:26
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('bo', '0015_course'),
    ]

    operations = [
        migrations.AddField(
            model_name='course',
            name='location',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='bo.Location'),
        ),
        migrations.AddField(
            model_name='course',
            name='scolaryear',
            field=models.DecimalField(decimal_places=0, max_digits=4, null=True),
        ),
    ]
