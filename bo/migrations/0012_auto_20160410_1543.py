# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bo', '0011_auto_20160407_1052'),
    ]

    operations = [
        migrations.AddField(
            model_name='suivis',
            name='estim_social',
            field=models.DecimalField(default=0, max_digits=1, decimal_places=0, choices=[(0, b'Inconnue'), (1, b'Bonne'), (2, b'Correcte'), (3, b'Probl\xc3\xa9matique')]),
        ),
        migrations.AddField(
            model_name='suivis',
            name='estim_tech',
            field=models.DecimalField(default=0, max_digits=1, decimal_places=0, choices=[(0, b'Inconnu'), (1, b'Tr\xc3\xa8s bon'), (2, b'Bon'), (3, b'Moyen'), (4, b'Faible'), (5, b'Tr\xc3\xa8s faible')]),
        ),
    ]
